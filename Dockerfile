FROM node:10.15.1
WORKDIR /app/front

# ARG PUBLIC_API_URL
# ENV BASE_URL $PUBLIC_API_URL

COPY package.json ./
COPY package-lock.json ./

RUN npm install

COPY . .
RUN npm run build
