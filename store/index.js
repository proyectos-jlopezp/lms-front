export const state = () => ({
  usuario: null,
  drawer: true,
  anios: [],
  aulas: [],
  profesores: [],
  administradores: [],
  alumnos: [],
  areas: [],
  cursos: [],
  token: null,
  aulasProfesor: [],
  horarioProfesor: [],
  diaProfesor: [],
  horarioAlumno: [],
  diaAlumno: [],
  cursosAlumno: [],
  subperiodos: [],
  subPeriodoActual: null,
  configuracion: null
});

export const getters = {
  getUsuario: state => state.usuario,
  getAulas: state => state.aulas,
  getAnios: state => state.anios,
  getProfesores: state => state.profesores,
  getAlumnosNuevos: state => state.alumnos,
  getAlumnosNuevosValidar: state => state.alumnos,
  getAlumnos: state => state.alumnos,
  getAreas: state => state.areas,
  getCursos: state => state.cursos,
  getAulasProfesor: state => state.aulasProfesor,
  getHorarioProfesor: state => state.horarioProfesor,
  getDiaProfesor: state => state.diaProfesor,
  getHorarioAlumno: state => state.horarioAlumno,
  getDiaAlumno: state => state.diaAlumno,
  getContenido: state => state.contenido,
  getCursosAlumno: state => state.cursosAlumno,
  getSubperiodos: state => state.subperiodos,
  getSubperiodoActual: state => state.subPeriodoActual,
  getConfiguracion: state => state.configuracion,
  getAdministradores: state => state.administradores
};

export const actions = {
  setToken({ commit }, { token, expiresIn }) {
    this.$axios.setToken(token, "refresh_token");
    const expiryTime = new Date(new Date().getTime() + expiresIn * 1000);

    this.$cookies.set('refresh_token', token, {
      path: '/',
      maxAge: expiryTime
    })
    
    commit("SET_TOKEN", token);
  },

  setUsuario(state, body) {
    state.commit("setUsuario", body);
  },

  async refreshToken({ dispatch }) {
    const { token, expiresIn } = await this.$axios.$post(`refresh-token`);
    dispatch("setToken", { token, expiresIn });
  },

  async logout({ commit }) {
    this.$axios.setToken(false);
    this.$cookies.removeAll()
    commit("REMOVE_TOKEN");
    localStorage.clear();
  },

  async logoutPost(state, body) {
    let response = await this.$axios.$post(`logout`);
    return response;
  },

  async changePassword(state, body) {
    let response = await this.$axios.$post(`usuario/password`, body);
    return response;
  },

  async getAulas(state) {
    let response = await this.$axios.$get(`aula/all`);
    state.commit("setAulas", response.body);
  },

  async validateSchedule(state, body) {
    return await this.$axios.$post(`aula/validacion`, body);
  },

  async getAula(state, id) {
    let response = await this.$axios.$get(`aula/${id}`);
    return response;
  },

  async deleteAula(state, id) {
    let response = await this.$axios
      .$get(`aula/${id}/delete`)
      .then(res => {
        return res;
      })
      .catch(err => {
        return err.response.data;
      });
    await state.dispatch("getAulas");
    return response;
  },

  async getCursosFromStudent(state, id) {
    let response = await this.$axios.$get(`alumno/cursos/${id}`);
    response.body.map(r => {
      if (r.imagen) {
        var backServer = this.$config.baseUrl.slice(0, -4);
        r.imagen = [backServer, r.imagen].join("/");
      }
    });
    state.commit("setCursosAlumno", response.body);
  },

  async getParticipantesFromStudent(state, id) {
    let response = await this.$axios.$get(`alumno/participantes/${id}`);
    return response.body;
  },

  async addAulaData(state, body) {
    console.log(body);
    let response = await this.$axios.$post(`aula`, body);
    return response;
  },

  async addAulaAlumnos(state, body) {
    console.log(body);
    let response = await this.$axios.$post(`aula/alumnos`, body);
    return response;
  },

  async addAulaCursos(state, body) {
    console.log(body);
    let response = await this.$axios.$post(`aula/cursos`, body);
    return response;
  },

  async getNivel() {
    let response = await this.$axios.$get(`nivel/all`);
    return response;
  },
  async getGrado() {
    let response = await this.$axios.$get(`grado/all`);
    return response;
  },

  async getProfesoresActivos() {
    let response = await this.$axios.$get(`profesor/activos`);
    return response;
  },

  async getCursosActivos() {
    let response = await this.$axios.$get(`curso/activos`);
    return response;
  },

  async getAniosAula() {
    let response = await this.$axios.$get(`anio/all`);
    return response;
  },

  async getAnios(state) {
    let response = await this.$axios.$get(`anio/all`);
    console.log(response.body);
    state.commit("setAnios", response.body);
  },

  async deletePeriodo(state, id) {
    let response = await this.$axios
      .$get(`anio/delete/${id}`)
      .then(res => {
        return res;
      })
      .catch(err => {
        return err;
      });
    return response;
  },

  async addAnio(state, body) {
    let response = await this.$axios.$post(`periodo/add`, body);
    await state.dispatch("getAnios");
    return response;
  },

  async editPeriodo(state, body) {
    let response = await this.$axios.$post(`anio`, body);
    await state.dispatch("getAnios");
    return response;
  },

  async actualAnio(state, body) {
    let response = await this.$axios.$get(`anio/actual/${body.periodoId}`);
    response = await this.$axios.$get(`periodo/actual/${body.subperiodoId}`);
    await state.dispatch("getAnios");
    return response;
  },

  async getAdministradores(state) {
    let response = await this.$axios.$get(`admin/all`);
    state.commit("setAdministradores", response.body);
  },

  //TODO: eliminar admin, editar admin

  async addAdmin(state, body) {
    let formData = new FormData();
    if (body.imageBlob) {
      formData.append("img", body.imageBlob, body.imageBlob.name);
    }
    formData.append("data", JSON.stringify(body));

    let response = await this.$axios.$post(`admin/add`, formData, {
      headers: {
        "Content-Type": "multipart/form-data"
      }
    });

    return response;
  },

  async editAdmin(state, body) {
    let formData = new FormData();
    if (body.imageBlob) {
      formData.append("img", body.imageBlob, body.imageBlob.name);
    }
    formData.append("data", JSON.stringify(body));

    let response = await this.$axios.$post(`admin/edit`, formData, {
      headers: {
        "Content-Type": "multipart/form-data"
      }
    });
    await state.dispatch("getAdministradores");

    return response;
  },

  async eliminarAdmin(state, id) {
    let response = await this.$axios.$get(`admin/delete/${id}`);
    await state.dispatch("getAdministradores");
    return response;
  },

  async getProfesores(state) {
    let response = await this.$axios.$get(`profesor/all`);
    state.commit("setProfesores", response.body);
  },
  async getAlumnosNuevos(state) {
    let response = await this.$axios.$get(`alumno/nuevos`);
    return response;
  },
  async getAlumnosNuevosValidar(state) {
    let response = await this.$axios.$get(`alumno/validateImport`);
    return response;
  },
  async getAlumnos(state) {
    let response = await this.$axios.$get(`alumno/all`);
    state.commit("setAlumnos", response.body);
  },
  async getOneUsuario(state, id) {
    let response = await this.$axios.$get(`usuario/one/${id}`);
    return response;
  },
  async addAlumno(state, body) {
    let formData = new FormData();
    if (body.imageBlob) {
      formData.append("img", body.imageBlob, body.imageBlob.name);
    }
    formData.append("data", JSON.stringify(body));
    let response = await this.$axios.$post(`alumno/add`, formData, {
      headers: {
        "Content-Type": "multipart/form-data"
      }
    });
    return response;
  },

  async editAlumno(state, body) {
    let formData = new FormData();
    if (body.imageBlob) {
      formData.append("img", body.imageBlob, body.imageBlob.name);
    }
    formData.append("data", JSON.stringify(body));

    let response = await this.$axios.$post(`alumno/edit`, formData, {
      headers: {
        "Content-Type": "multipart/form-data"
      }
    });
    await state.dispatch("getAlumnos");

    return response;
  },

  async addProfesor(state, body) {
    let formData = new FormData();
    if (body.imageBlob) {
      formData.append("img", body.imageBlob, body.imageBlob.name);
    }
    formData.append("data", JSON.stringify(body));

    let response = await this.$axios.$post(`profesor/add`, formData, {
      headers: {
        "Content-Type": "multipart/form-data"
      }
    });

    return response;
  },

  async editProfesor(state, body) {
    let formData = new FormData();
    if (body.imageBlob) {
      formData.append("img", body.imageBlob, body.imageBlob.name);
    }
    formData.append("data", JSON.stringify(body));

    let response = await this.$axios.$post(`profesor/edit`, formData, {
      headers: {
        "Content-Type": "multipart/form-data"
      }
    });
    await state.dispatch("getProfesores");

    return response;
  },

  async eliminarProfesor(state, id) {
    let response = await this.$axios.$get(`profesor/delete/${id}`);
    await state.dispatch("getProfesores");
    return response;
  },

  async getHorarioProfesor(state, id) {
    let response = await this.$axios.$get(`profesor/horario/${id}`);
    state.commit("setHorarioProfesor", response.body);

    return response;
  },
  async getDiaProfesor(state, id) {
    let response = await this.$axios.$get(`profesor/midia/${id}`);

    state.commit("setDiaProfesor", response.body);
    return response;
  },

  async getHorarioAlumno(state, id) {
    let response = await this.$axios.$get(`alumno/horario/${id}`);
    state.commit("setHorarioAlumno", response.body);

    return response;
  },

  async getDiaAlumno(state, id) {
    let response = await this.$axios.$get(`alumno/midia/${id}`);

    state.commit("setDiaAlumno", response.body);
    return response;
  },

  async eliminarAlumno(state, id) {
    let response = await this.$axios.$get(`alumno/delete/${id}`);
    await state.dispatch("getAlumnos");
    return response;
  },

  async getApoderado(state, id) {
    let response = await this.$axios.$get(`alumno/apoderado/${id}`);
    return response.body;
  },

  async getAreas(state) {
    let response = await this.$axios.$get(`area/all`);
    state.commit("setAreas", response.body);
  },

  async addArea(state, body) {
    let response = await this.$axios.$post(`area/add`, body, {
      headers: {
        //"Content-Type": "multipart/form-data"
      }
    });

    await state.dispatch("getAreas");

    return response;
  },

  async editArea(state, body) {
    let response = await this.$axios.$post(`area/edit`, body, {
      headers: {
        //"Content-Type": "multipart/form-data"
      }
    });

    await state.dispatch("getAreas");

    return response;
  },

  async resetPassword(state, body) {
    let response = await this.$axios.$post(`usuario/resetPassword`, body);
    return response;
  },

  async getCursos(state, body) {
    let response = await this.$axios.$get(`curso/all?idArea=` + body.idArea);

    state.commit("setCursos", response.body);
    return JSON.parse(JSON.stringify(response.body));
  },

  async addCurso(state, body) {
    console.log(body);
    let formData = new FormData();
    if (body.imageBlob) {
      formData.append("img", body.imageBlob, body.imageBlob.name);
    }
    formData.append("data", JSON.stringify(body));
    console.log(formData);
    let response = await this.$axios
      .$post(`curso/add`, formData, {
        headers: {
          "Content-Type": "multipart/form-data"
        }
      })
      .then(r => {
        return r;
      })
      .catch(e => {
        return e;
      });
    console.log(response);

    await state.dispatch("getCursos", { idArea: body.area_id });

    return response;
  },

  async eliminarCurso(state, body) {
    let response = await this.$axios.$post(`curso/eliminar`, body);
    state.commit("setCursos", response.body);

    await state.dispatch("getCursos", { idArea: body.area_id });

    return response;
  },

  async editCurso(state, body) {
    let formData = new FormData();
    if (body.imageBlob) {
      formData.append("img", body.imageBlob, body.imageBlob.name);
    }
    formData.append("data", JSON.stringify(body));
    let response = await this.$axios.$post(`curso/editar`, formData, {
      headers: {
        "Content-Type": "multipart/form-data"
      }
    });

    await state.dispatch("getCursos", { idArea: body.area_id });

    return response;
  },

  async getAulasProfesor(state, body) {
    let response = await this.$axios.$get(
      `profesor/grados?idProfesor=` + body.idProfesor
    );
    state.commit("setAulasProfesor", response.body);
    return response.body;
  },

  async addContenido(state, body) {
    let response = await this.$axios.$post(`curso/saveContenido`, body, {
      headers: {
        //"Content-Type": "multipart/form-data"
      }
    });
    await state.dispatch("getContenido", { idAulaCurso: body.idCursoAula });

    return response;
  },

  async addCarpeta(state, body) {
    let response = await this.$axios.$post(`curso/addCarpeta`, body, {
      headers: {
        //"Content-Type": "multipart/form-data"
      }
    });
    await state.dispatch("getContenido", { idAulaCurso: body.idCursoAula });

    return response;
  },

  async getContenido(state, body) {
    let response = await this.$axios.$get(
      `curso/getContenido?idAulaCurso=` + body.idAulaCurso
    );

    state.commit("setContenido", response.body);
    return response.body;
  },

  async deleteContenido(state, body) {
    let response = await this.$axios.$post(`curso/deleteContenido`, body, {
      headers: {
        //"Content-Type": "multipart/form-data"
      }
    });
    await state.dispatch("getContenido", { idAulaCurso: body.idCursoAula });

    return response;
  },

  async actualizarCarpeta(state, body) {
    let response = await this.$axios.$post(`curso/actualizarCarpeta`, body, {
      headers: {
        //"Content-Type": "multipart/form-data"
      }
    });
    await state.dispatch("getContenido", { idAulaCurso: body.idCursoAula });

    return response;
  },

  async getSubperiodos(state) {
    let response = await this.$axios.$get(`anio/getActual`);
    state.commit("setSubperiodos", response.body);

    var subperiodos = response.body.subperiodos;

    var subPeriodoActual = subperiodos.find(obj => {
      if (obj.activo) return true;
    });

    state.commit("setSubPeriodoActual", subPeriodoActual.secuencia - 1);

    return response;
  },

  async getContenido(state, body) {
    let response = await this.$axios.$get(
      `curso/getContenido?idAulaCurso=` + body.idAulaCurso
    );

    state.commit("setContenido", response.body);
    return response.body;
  },

  async getConfiguracion(state, body) {
    let response = await this.$axios.$get(`configuracion/all`);
    state.commit("setConfiguracion", response.body);

    return response.body;
  },

  async updateConfiguracion(state, body) {
    let formData = new FormData();

    if (body.imageBlob && body.cambioLogoBo) {
      formData.append("img", body.imageBlob, body.imageBlob.name);
    }
    if (body.imageBlobLogin && body.cambioInicioBo) {
      formData.append(
        "img_login",
        body.imageBlobLogin,
        body.imageBlobLogin.name
      );
    }
    formData.append("data", JSON.stringify(body));
    let response = await this.$axios.$post(`configuracion/edit`, formData, {
      headers: {
        "Content-Type": "multipart/form-data"
      }
    });

    return response;
  },

  async login(state, body) {
    let response = await this.$axios.$post(`login`, body);

    return response;
  }
};

export const mutations = {
  SET_TOKEN(state, token) {
    state.token = token;
  },

  REMOVE_TOKEN(state) {
    state.token = null;
  },

  setUsuario(state, val) {
    state.usuario = val;
  },

  setAulas(state, val) {
    state.aulas = val;
  },
  setAnios(state, val) {
    state.anios = val;
  },
  setAlumnos(state, val) {
    state.alumnos = val;
  },
  setCursosAlumno(state, val) {
    state.cursosAlumno = val;
  },
  toggleDrawer(state) {
    state.drawer = !state.drawer;
  },
  drawer(state, val) {
    state.drawer = val;
  },
  setProfesores(state, val) {
    state.profesores = val;
  },
  setAdministradores(state, val) {
    state.administradores = val;
  },
  setAreas(state, val) {
    state.areas = val;
  },
  setCursos(state, val) {
    state.cursos = val;
  },
  setAulasProfesor(state, val) {
    state.aulasProfesor = val;
  },
  setHorarioProfesor(state, val) {
    state.horarioProfesor = val;
  },
  setDiaProfesor(state, val) {
    state.diaProfesor = val;
  },
  setHorarioAlumno(state, val) {
    state.horarioAlumno = val;
  },
  setDiaAlumno(state, val) {
    state.diaAlumno = val;
  },
  setContenido(state, val) {
    state.contenido = val;
  },
  setSubperiodos(state, val) {
    state.subperiodos = val;
  },
  setSubPeriodoActual(state, val) {
    state.subPeriodoActual = val;
  },
  setConfiguracion(state, val) {
    state.configuracion = val;
  }
};
