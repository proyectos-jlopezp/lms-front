const pkg = require("./package");
require("dotenv").config();
const VuetifyLoaderPlugin = require("vuetify-loader/lib/plugin");

var webpack = require("webpack");
var path = require("path");

export default {
  components: true,
  mode: "spa",

  /*
   ** Headers of the page
   */
  head: {
    title: "Eduqy Plataforma",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content:
          "Eduqy Plataforma: bienvenido a una mejor \n" +
          " experiencia de aprendizaje."
      }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      {
        rel: "stylesheet",
        href:
          "https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons"
      },
      {
        rel: "stylesheet",
        href:
          "https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
      }
    ],
    script: [
      {
        src:
          "https://cdnjs.cloudflare.com/ajax/libs/echarts/4.0.4/echarts-en.min.js"
      }
    ]
  },

  /*
   ** Customize the progress-bar color
   */
  loading: { color: "#3adced" },

  /*
   ** Global CSS
   */
  css: [
    "~/assets/style/theme.styl",
    "~/assets/style/app.styl",
    "~/assets/main.css",
    "~/assets/transitions.css",
    "font-awesome/css/font-awesome.css",
    "roboto-fontface/css/roboto/roboto-fontface.css"
  ],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    "@/plugins/vuetify",
    "@/plugins/vee-validate",
    '~/plugins/axios',
  ],


  /*
   ** Nuxt.js modules
   */
  modules: [
    "@nuxtjs/axios",
    "@nuxtjs/dotenv",
    "nuxt-sweetalert2",
    "@nuxtjs/toast",
    ["cookie-universal-nuxt", { parseJSON: false }],
  ],
  // env: {
  //   baseUrl: "http://52.201.197.72:8000/api",
  //   credentials: true
  // },
  axios: {
    baseURL: "http://localhost:8000/api",
    credentials: false, // this says that in the request the httponly cookie should be sent  },
  },
  publicRuntimeConfig: {
    axios: {
      browserBaseURL: process.env.BASE_URL
    },
    baseUrl: process.env.BASE_URL || "http://localhost:8000/api"
  },

  router: {
    middleware: 'refreshToken',
  },

  auth: {
    redirect: {
      login: "/login",
      logout: "/",
      callback: "/login",
      home: "/index"
    },
    strategies: {
      local: false,
      password_grant: {
        _scheme: "local",
        endpoints: {
          login: {
            url: "/oauth/token",
            method: "post",
            propertyName: "access_token"
          },
          logout: false,
          user: {
            url: "api/auth/me",
            method: "get",
            propertyName: "user"
          }
        }
      }
    }
  },

  /*
   ** Build configuration
   */
  build: {
    transpile: ["vuetify/lib"],
    plugins: [
      new VuetifyLoaderPlugin(),
      new webpack.ProvidePlugin({
        $: "jquery",
        jQuery: "jquery"
      })
    ],
    loaders: {
      stylus: {
        import: ["~assets/style/variables.styl"]
      }
    },

    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {}
  }
};
