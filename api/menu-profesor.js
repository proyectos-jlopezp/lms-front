const MenuProfesores = [
  { header: "Menú Profesores" },
  {
    title: "Mi Día",
    group: "Profesor",
    icon: "today",
    name: "Mi día",
    href: "/profesor/midia"
  },
  {
    title: "Calendario",
    group: "Profesor",
    icon: "calendar_today",
    name: "Calendario",
    href: "/profesor/calendario"
  },
  {
    title: "Mis cursos",
    group: "Profesor",
    icon: "library_books",
    name: "Mis cursos",
    href: "/profesor/miscursos"
  },
  // TODO: Pendiente el Nivel de logro
  //{
  // title: "Nivel de logro",
  //  group: "Profesor",
  //  icon: "grading",
  //  name: "Nivel de logro",
  //  href: "/profesor/logro"
  //},
  { divider: true },
  { header: "Opciones" },
  {
    title: "Cerrar Sesión",
    group: "extra",
    icon: "login",
    href: "/logout"
  },

  { divider: true }
];
// reorder menu
MenuProfesores.forEach(item => {
  if (item.items) {
    item.items.sort((x, y) => {
      let textA = x.title.toUpperCase();
      let textB = y.title.toUpperCase();
      return textA < textB ? -1 : textA > textB ? 1 : 0;
    });
  }
});

export default MenuProfesores;
