const MenuAlumno = [
  { header: "Alumno" },
  {
    title: "Mi Día",
    group: "Alumno",
    icon: "today",
    name: "Mi día",
    href: "/alumno/midia"
  },
  {
    title: "Horario",
    group: "Alumno",
    icon: "calendar_today",
    name: "Calendario",
    href: "/alumno/calendario"
  },
  {
    title: "Mis Cursos",
    group: "Alumno",
    icon: "library_books",
    name: "Cursos",
    href: "/alumno/cursos/lista"
  },
  {
    title: "Participantes",
    group: "Alumno",
    icon: "people",
    name: "Participantes",
    href: "/alumno/participantes/lista"
  },

  { divider: true },
  { header: "Opciones" },
  {
    title: "Cerrar Sesión",
    group: "extra",
    icon: "login",
    href: "/logout"
  },
  { divider: true }
];
// reorder menu
MenuAlumno.forEach(item => {
  if (item.items) {
    item.items.sort((x, y) => {
      let textA = x.title.toUpperCase();
      let textB = y.title.toUpperCase();
      return textA < textB ? -1 : textA > textB ? 1 : 0;
    });
  }
});

export default MenuAlumno;
