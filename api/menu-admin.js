const MenuAdmin = [
  { header: "Administrador" },
  {
    title: "Periodos",
    group: "Administrador",
    icon: "date_range",
    name: "Periodos",
    href: "/admin/periodo"
  },
  {
    title: "Alumnos",
    group: "Administrador",
    icon: "groups",
    name: "Mantenimiento Alumnos",
    href: "/admin/alumnos/lista",
    props: true
  },
  {
    title: "Profesores",
    group: "Administrador",
    icon: "school",
    name: "Mantenimiento Profesores",
    href: "/admin/profesores/lista"
  },
  {
    title: "Áreas y Cursos",
    group: "Administrador",
    icon: "library_books",
    name: "Areas",
    href: "/admin/areas/area"
  },
  {
    title: "Aulas",
    group: "Administrador",
    icon: "meeting_room",
    name: "Aulas",
    href: "/admin/aulas/lista",
    props: true
  },

  {
    title: "Administradores",
    group: "Administrador",
    icon: "engineering",
    name: "Administradores",
    href: "/admin/administradores/lista",
    props: true
  },

  { divider: true },
  { header: "Opciones" },
  {
    title: "Configuración Eduqy",
    group: "Administrador",
    icon: "build",
    name: "Configuracion",
    href: "/admin/configuracion/lista",
    props: true
  },
  {
    title: "Cerrar Sesión",
    group: "extra",
    icon: "login",
    href: "/logout"
  },

  { divider: true }
];
// reorder menu
MenuAdmin.forEach(item => {
  if (item.items) {
    item.items.sort((x, y) => {
      let textA = x.title.toUpperCase();
      let textB = y.title.toUpperCase();
      return textA < textB ? -1 : textA > textB ? 1 : 0;
    });
  }
});

export default MenuAdmin;
