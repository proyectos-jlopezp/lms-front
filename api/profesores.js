const Profesores = [
  {
    id: 1,
    nombre: "Teddy Campos",
    estado: "Matriculado",
    telefono: "4223341",
    correo_ap: "absullon@gmail.com"
  },
  {
    id: 2,
    nombre: "Gonchi Gonzalo",
    estado: "No Matriculado",
    telefono: "4223341",
    correo_ap: "absullon@gmail.com"
  },
  {
    id: 3,
    nombre: "Johnny Jorge Lopez",
    estado: "Matriculado",
    telefono: "4223341",
    correo_ap: "absullon@gmail.com"
  },
  {
    id: 4,
    nombre: "Emerson Yepez",
    estado: "Matriculado",
    telefono: "4223341",
    correo_ap: "absullon@gmail.com"
  },
  {
    id: 5,
    nombre: "Josue Tyson",
    estado: "Matriculado",
    telefono: "4223341",
    correo_ap: "absullon@gmail.com"
  }
];

export { Profesores };
