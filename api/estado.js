const nuevoAlumno = {
  id: 7,
  valor: "NUEVO"
};

const EstadoAlumno = [
  {
    id: 0,
    valor: "TODOS"
  },
  {
    id: nuevoAlumno.id,
    valor: nuevoAlumno.valor
  },
  {
    id: 1,
    valor: "MATRICULADO"
  },
  {
    id: 2,
    valor: "NO MATRICULADO"
  },
  {
    id: 3,
    valor: "RETIRADO"
  },
  {
    id: 4,
    valor: "EXPULSADO"
  },
  {
    id: 8,
    valor: "SUSPENDIDO"
  }
];

const EstadosEdicionAlumno = [
  {
    id: 1,
    valor: "MATRICULADO"
  },

  {
    id: 3,
    valor: "RETIRADO"
  },
  {
    id: 4,
    valor: "EXPULSADO"
  },
  {
    id: 8,
    valor: "SUSPENDIDO"
  }
];

const EstadosRegistroAlumno = [nuevoAlumno];
const EstadoNuevoAlumno = nuevoAlumno;
const EstadoProfesor = [
  {
    id: 0,
    valor: "TODOS"
  },
  {
    id: 5,
    valor: "ACTIVO"
  },
  {
    id: 6,
    valor: "NO ACTIVO"
  }
];

export {
  EstadoAlumno,
  EstadoProfesor,
  EstadosRegistroAlumno,
  EstadosEdicionAlumno,
  EstadoNuevoAlumno,
  nuevoAlumno
};
