export default function({ app, store, redirect }) {
  
  if (!app.$cookies.get('refresh_token') ) {
    console.log('Redireccionando login...ingrese nuevamente');
    return redirect("/login");
  }

  if(!localStorage.getItem("usuario")){
    console.log('Redireccionando login...ingrese nuevamente, no hay usuario');
    return redirect("/login");
  }else{
    store.dispatch("setUsuario", JSON.parse(localStorage.getItem("usuario")));
  }

}
