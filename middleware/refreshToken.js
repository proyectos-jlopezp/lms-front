export default function ({ app, store, redirect }) {
  
  const token = app.$cookies.get('refresh_token');

  if (! token) {
    store.dispatch('refreshToken')
      .catch(errors => {        
        store.dispatch('logout');
      });
  }
}
