export default function ({ store, redirect }) {
  var user = JSON.parse(localStorage.getItem("usuario"));
  if (!user) {
    return redirect("/login");
  } else if (user.tipo_usuario.nombre !== "ALUMNO") {
    console.log("Usuario no permitido");
    return redirect("/404");
  } else {
    console.log("Usuario permitido.");
  }
}
