export default class UtilImport{
    static getCharSeparator(line){
        let charSeparator = "";
        if(line.indexOf(",")>=0)
          charSeparator = ",";
        if(line.indexOf(";")>=0)
          charSeparator = ";";
        return charSeparator;
    }
}