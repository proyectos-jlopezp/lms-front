export default ({ app, $axios, env }) => {
    if(app.$cookies.get('refresh_token')){   
        $axios.onRequest(config => {
            config.headers.common['Authorization'] = `refresh_token= `+ app.$cookies.get('refresh_token');
        });
    }
    
  }