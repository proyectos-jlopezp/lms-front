import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import MultiFiltersPlugin from './MultiFilters'


Vue.use(MultiFiltersPlugin);
Vue.use(Vuetify, {customProperties: true})
